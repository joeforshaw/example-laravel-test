<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Estimates</title>
  </head>
  <body>
    <h1>Delivery estimator</h1>
    {{ Form::open(['route' => 'estimates.store']) }}
      <div class="form-group">
        {{ Form::label('date') }}
        {{ Form::input('datetime-local', 'date') }}
      </div>

      <div class="form-group">
        {{ Form::label('country', 'Country') }}
        {!! Countries::getList('en', 'html') !!}
      </div>

      {{ Form::submit('Submit', ['class' => 'btn btn-info']) }}
    {{ Form::close() }}

    @if ($estimate)
      <h2>Estimated delivery date: {{ $estimate->format('d/m/Y') }}</h2>
    @endif
  </body>
</html>
