<?php

namespace App\Http\Controllers;

use App\DeliveryDateEstimator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EstimatesController extends Controller
{
    public function index(Request $request)
    {
        $estimate = $request->has('estimate')
            ? Carbon::parse($request->input('estimate'))
            : null;

        return view('estimates.index', ['estimate' => $estimate]);
    }

    public function store(Request $request)
    {
        $estimator = new DeliveryDateEstimator();

        $estimateDate = $estimator->calculate(
            Carbon::parse($request->input('date')),
            $request->input('value')
        );

        return redirect()->route('estimates.index', [
            'estimate' => $estimateDate->format('Y-m-d')
        ]);
    }
}
