<?php

namespace App;

class DeliveryDateEstimator
{
    private static $europeanCountries = [
        'AT', 'BE', 'BG', 'CY', 'CZ', 'DK', 'EE',
        'FI', 'FR', 'DE', 'GR', 'HU', 'IE', 'IT',
        'LV', 'LT', 'LU', 'MT', 'NL', 'PL', 'PT',
        'RO', 'SK', 'SI', 'ES', 'SE', 'GB'
    ];

    public function calculate($deliverFor, $country)
    {
        $estimate = $deliverFor;

        if ($this->past4pm($estimate)) {
            $estimate->addDay();
        }

        if ($this->inUK($country)) {
            $estimate->addDays(1);
        } else if ($this->inEurope($country)) {
            $estimate->addDays(3);
        } else {
            $estimate->addDays(8);
        }

        if ($estimate->isWeekend()) {
            $estimate = $this->nextWeekday($estimate);
        }

        return $estimate->startOfDay();
    }

    private function nextWeekday($date)
    {
        return $date->startOfWeek()->addWeek(1);
    }

    private function past4pm($date)
    {
        return $date->format('H') > 15;
    }

    public function inUK($country)
    {
        return $country === 'GB';
    }

    public function inEurope($country)
    {
        return in_array(
            $country,
            static::$europeanCountries
        );
    }
}
