<?php

namespace Tests;

use App\DeliveryDateEstimator;
use App\Models\Address;
use Carbon\Carbon;

class DeliveryDateEstimatorTest extends TestCase
{
    private $estimator, $address, $targetDelivery;

    public function setUp()
    {
        parent::setUp();

        $this->estimator = new DeliveryDateEstimator();
                
        $this->address = new Address();
        $this->address->country = 'GB';
    }

    public function testIsAlwaysAWeekday()
    {
        $this->targetDelivery = Carbon::parse('2019-06-07 09:00:00'); // Friday
        $this->assertEquals(
            Carbon::parse('2019-06-10'), // Monday
            $this->runEstimator()
        );
    }

    public function testWhenOrderingDuringTheDay()
    {
        $this->targetDelivery = Carbon::parse('2019-06-03 09:00:00');
        $this->assertEquals(
            Carbon::parse('2019-06-04'),
            $this->runEstimator()
        );
    }

    public function testWhenOrderingAtNight()
    {
        $this->targetDelivery = Carbon::parse('2019-06-03 20:00:00');
        $this->assertEquals(
            Carbon::parse('2019-06-05'),
            $this->runEstimator()
        );
    }

    public function testWhenOrderingInUK()
    {
        $this->address->country = 'GB';
        $this->targetDelivery = Carbon::parse('2019-06-03 09:00:00');
        $this->assertEquals(
            Carbon::parse('2019-06-04'),
            $this->runEstimator()
        );
    }

    public function testWhenOrderingInEurope()
    {
        $this->address->country = 'FR';
        $this->targetDelivery = Carbon::parse('2019-06-03 09:00:00');
        $this->assertEquals(
            Carbon::parse('2019-06-06'),
            $this->runEstimator()
        );
    }

    public function testWhenOrderingOutsideOfEurope()
    {
        $this->address->country = 'US';
        $this->targetDelivery = Carbon::parse('2019-06-03 09:00:00');
        $this->assertEquals(
            Carbon::parse('2019-06-11'),
            $this->runEstimator()
        );
    }

    private function runEstimator()
    {
        return $this->estimator->calculate(
            $this->targetDelivery,
            $this->address
        );
    }
}
